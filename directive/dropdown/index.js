angular
    .module('Mydropdown',[])
    .directive('dropdownSmpl', function() {
        return {
        restrict: 'AE',
        replace: true,
        template: '<div class="btn-group ddl" >' +
        '    <button type="button" class="btn  dropdown-toggle {{isPlaceholder?\'ddbg-default\':\'ddbg-selected\'}}" data-toggle="dropdown" aria-haspopup="true" ng-model="selected" aria-expanded="true">' +
        '    {{display}} <span class="caret"></span>' +
        '    </button>' +
        '    <ul class="dropdown-menu">' +
        '    <li class="dropdown-header">{{placeholder}}</li>'+
        '    <li ng-repeat="l in list" ng-click="select(l)"><a href="#" >{{l}}</a></li>' +
        '  </ul>' +
        '  </div>',

        scope: {
            list : "=",
            selected:"=",
            placeholder:"@"
        },
        link: function(scope) {
            if(scope.list.indexOf(scope.selected)>=0){
                scope.display=scope.selected;
            }else {
                scope.display=scope.placeholder;
                scope.isPlaceholder = true;
            }


            scope.select = function(item) {
                scope.isPlaceholder = false;
                scope.display = item;
                scope.selected =item;
            };


        }
    };
})
    .directive('dropdownAdv', function() {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div class="btn-group ddl" >' +
            '    <button type="button" class="btn  dropdown-toggle {{isPlaceholder?\'ddbg-default\':\'ddbg-selected\'}}" data-toggle="dropdown" aria-haspopup="true" ng-model="selected" aria-expanded="true">' +
            '    {{display}} <span class="caret"></span>' +
            '    </button>' +
            '    <ul class="dropdown-menu">' +
            '    <li class="dropdown-header">{{placeholder}}</li>'+
            '    <li ng-repeat="l in list" ng-click="select(l,l.driver.name+\'/\'+l.car.no)"><a href="#" class="wrapper" >{{l.driver.name}}/{{l.car.no}}' +
            '<div class="tooltipd" >' +
            '<div ng-if="l.driver" style="width: 300px;float: left"><p>Driver</p><img style="float: left;width: 50px;" ng-src="{{l.driver.img}}"><span >{{l.driver.name}}</span></div>' +
            '<div  ng-if="l.car" style="width: 300px;float: left"><p >Vehicle</p><img style="float: left;width: 50px;" ng-src="{{l.car.img}}"><span>{{l.car.no}}</span></div>' +
            '</div>' +
            '</a></li>' +
            '  </ul>' +
            '  </div>',

            scope: {
                list : "=",
                selected:"=",
                placeholder:"@"
            },
            link: function(scope) {
                if(scope.list.indexOf(scope.selected)>=0){
                    scope.display=scope.selected;
                }else {
                    scope.display=scope.placeholder;
                    scope.isPlaceholder = true;
                }


                scope.select = function(item,x) {
                    scope.isPlaceholder = false;
                    scope.display = x;
                    scope.selected =item;
                };


            }
        };
    });