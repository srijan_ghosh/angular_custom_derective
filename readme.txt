Documentation...........................

1.This is Custom Angular Directive.
    There are two different types of Dropdown list
    i)dropdown-smpl
    ii)dropdown-adv

2.Run index.html for demo.

3.Useage
    i)Dependencies :
        1.bootstrap
                <link type="text/css" rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
                <script  src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
                <script  src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>

                                        or use your own bower component


    ii)Include Libray:
        1.Include directive folder to your project
        2.link libray to your project
            css
                <link type="text/css" rel="stylesheet" href="directive/dropdown/style.css">

            js
                <script  src="directive/dropdown/index.js"></script>

        3.include dependency to your angular module as "Mydropdown"
            angular.module('myapp', ['Mydropdown']);


    iii)use in html:
        <dropdown-smpl list="ddl1" placeholder="Select Car" selected="sel1" ></dropdown-smpl>
        or
        <dropdown-adv list="ddl2" placeholder="Select Driver/Car" selected="sel2" ></dropdown-adv>

        1."list" is a model from where dropdown list will bind
        2."placeholder" is a text to display text as placeholder it connot be selected
        3."selected" is like ng-model for selecting or get selected model. two way binding.

    iv)Note:
        1. there is some predefined format for <list> model
            1.<dropdown-smpl list="listmodel" >
            $scope.listmodel=["simple text 1","simple text 2"];

        2.<dropdown-adv list="listmodel" >
            $scope.listmodel=[
                                     {
                                         driver:{
                                            name:"Hangla Therium",
                                            img:"https://ideas.sap.com/ct/getfile.bix?a=OD5268&f=BDE1D776-32E3-450D-87E6-979FA4A28D31&thumb=50"
                                         },
                                         car:{
                                            no:"WB-8009453",
                                            img:"http://www.popularmaruti.com/wp-content/themes/kuttup/timthumb.php?src=/wp-content/uploads/2016/03/Maruti-Suzuki-Alto.jpg&zc=1&w=50&h=50"
                                         }
                                     },
                                     {
                                         driver:{name:"Predo Doctor",img:"https://ideas.sap.com/ct/getfile.bix?a=OD5268&f=BEFEB04E-4DEA-4DE1-9A83-39437180C04F&thumb=50"}
                                     }
                              ];





